# Notes on the graphs

- Name: [alvin_search.html](alvin_search.html) and interactive version [here](
https://mardub.gitlab.io/libralinked/alvin_search.html )
- Data: [data_nocopyright.csv](data_nocopyright.csv)

# Description
This graph are is based on: [alvin_search.csv](alvin_search.csv) we applied different cleaning to it, mainly removing the licence, and removing the rune stone Sö 179
This file is based on all the shelfmarks of the following search on alvin:
[language= Old Swedish](https://www.alvin-portal.org/alvin/resultList.jsf?aq=%5B%5B%7B%22alpha3%22%3A%229fs%22%7D%5D%5D&faces-redirect=true&sortString=relevance_sort_desc&aqe=%5B%5D&af=%5B%5D&searchType=EXTENDED&noOfRows=250&dswid=-4674).

## Process (For developpers)
To re-run the scraping:
```bash
cd path/to/Alvin
python scrap_alvin.py
```
To run the graph. Take the dataset you wish to generate, for instance "place_manuscripts.csv"
In a terminal run :
```
cd path/to/alvin
sh clean_data.sh
python graph.py
```
Note: graph.R does not run in Rstudio.
