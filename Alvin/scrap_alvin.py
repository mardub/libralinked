"""
    scrap all the data from old swedish texts of alvin website
"""
import requests
from bs4 import BeautifulSoup
import time
#urlleft="https://www.alvin-portal.org/alvin/view.jsf?aq=[[{%22alpha3%22%3A%229fs%22}]]&c="
#urlright="&aqe=[]&af=[]&searchType=EXTENDED&pid=alvin-record%3A264493&dswid=9633#alvin-record%3A264493"
"https://www.alvin-portal.org/alvin/view.jsf?dswid=9413&pid=alvin-record%3A184808&c=14&searchType=EXTENDED&af=%5B%5D&query=&aq=%5B%5B%7B%22alpha3%22%3A%229fs%22%7D%5D%5D&aqe=%5B%5D"
search_urls = ["https://www.alvin-portal.org/alvin/resultList.jsf?aq=%5B%5B%7B%22alpha3%22%3A%229fs%22%7D%5D%5D&faces-redirect=true&sortString=relevance_sort_desc&aqe=%5B%5D&af=%5B%5D&searchType=EXTENDED&noOfRows=250&dswid=-4674"]

prefix="https://www.alvin-portal.org"
"""
Create a csv called alvin_search.csv with 4 columns:
"source,label_s,target,label_t"
"""
#open 'alvin_old.csv' in write mode
f= open("alvin_search.csv", "w")
f.write('source,label_s,target,label_t\n')
#open error file in write mode
g = open("error.csv", "w")
g.write('source,label_s,target,label_t\n')
def get_page(url):
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    return soup

def clean_commas(string):
    """
    Remove commas and newlines from a string.
    """
    return string.replace(',', '__').replace('\n', '').strip()


def get_search_content(soup):
    """make a soup only the <div id=alvinForm:recordPanel_content>"""
    alvinSearch = soup.find(id="alvinForm:recordPanel_content")
    return alvinSearch

"""make a soup of only the <div class="mainContainerWrapper">"""
def get_main_container(soup):
    main_container = soup.find(class_="mainContainerWrapper")
    return main_container

for search_url in search_urls:
    search_soup=get_search_content(get_page(search_url))
    search_links = search_soup.find_all("a")

    #Loop through the search results of alvin
    for link in search_links:
        if link.get('href')== None:
            print(link.get('href'))
            continue
        """get the url of the search entry"""
        url = prefix+link.get('href')
        page=requests.get(url)
        soup=BeautifulSoup(page.content,'html.parser')
        time.sleep(0.5)
        #get the title of the search page tag html h1
        if soup == None:
            continue
        h1=soup.find('h1')
        if h1 == None:
            continue
        title=h1.text
        """get the main container"""
        main_container=get_main_container(soup)
        for link in main_container.find_all('a'):
            #get the title of the page
            """get the href attribute,the text and the title"""
            href = link.get('href')
            #print(href)
            text = link.get_text()
            tit = link.get('title')
            if tit!=None:
                label_t= clean_commas(text+tit)
            else:
                label_t= clean_commas(text)
            
            source=clean_commas(url)
            label_s= clean_commas(title)
            target=clean_commas(href).lstrip()
            #if target starts with "/" 
            #if string target starts with "start":


            if "alvin" in target and not target.startswith("http"):
                target="https://www.alvin-portal.org"+target
            if "mageViewer" in target and label_t=="":
                label_t="image"
            if target == "#" or "Contact us" in label_t:
                continue
        
            raw=source+","+label_s+","+target+","+label_t
            f.write(raw+'\n')